//
//  ResumeModel.swift
//  MosKadr
//
//  Created by Ruslan on 10/23/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import ObjectMapper

class ResumeModel: Mappable, Age {

    var id: Int!
    var age: Int!
    var name: String!
    var date: String = ""
    var nationality: Int!
    var profession: [ProfessionModel]!
    var email: String = ""
    var experience: Int!
    var metro_id: Int?
    var phone: String = ""
    var foto: String = ""
    var sex: Int = 1
    var salary: Int = 0
    var vk_link: String = ""
    var fb_link: String = ""
    var ok_link: String = ""
    var about: String = ""
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        date    <- map["date"]
        name    <- map["name"]
        age    <- map["age"]
        nationality <- map["nationality"]
        profession <- map["profession"]
        email    <- map["email"]
        phone    <- map["phone"]
        about    <- map["about"]
        foto    <- map["foto"]
        sex    <- map["sex"]
        salary <- map["salary"]
        vk_link <- map["vk_link"]
        fb_link <- map["fb_link"]
        ok_link <- map["ok_link"]
        metro_id <- map["metro_id"]
        experience <- map["experience"]
    }
    
    func groups() -> [ProfessionGroupModel] {
        var groups: [ProfessionGroupModel] = []
        for group in AppManager.shared.options.profession_group {
            let tempGroup = ProfessionGroupModel.init()
            tempGroup.id = group.id
            tempGroup.name = group.name
            tempGroup.profession = []
            for profession in group.profession {
                for id in self.profession {
                    if profession.id == id.id {
                        tempGroup.profession.append(profession)
                    }
                }
            }
            if tempGroup.profession.count > 0 {
                groups.append(tempGroup)
            }
        }
        return groups
    }

}
