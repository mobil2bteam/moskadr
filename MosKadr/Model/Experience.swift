//
//  Experience.swift
//  MosKadr
//
//  Created by Ruslan on 10/22/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import Foundation

typealias ExperienceTuple = (id: Int, value: String, experience_from: String, experience_to: String)

class Experience {
    
    static let values = [
        ExperienceTuple(id:0, value: "Без опыта",  experience_from: "0", experience_to: "0"),
        ExperienceTuple(id:3, value: "до 1 года",  experience_from: "0", experience_to: "1"),
        ExperienceTuple(id:1, value: "1-3 года",  experience_from: "1", experience_to: "3"),
        ExperienceTuple(id:2, value: "3-6 года",  experience_from: "3", experience_to: "6"),
        ExperienceTuple(id:99, value: "от 6 до 9 лет",  experience_from: "6", experience_to: "9"),
        ExperienceTuple(id:100, value: "Более 9 лет",  experience_from: "9", experience_to: "")]
    
    

    
    static func getValues() -> [String] {
        var temp = [String]()
        for value in Experience.values {
            temp.append(value.value)
        }
        return temp
    }
    
    static func value(for id: Int) -> ExperienceTuple {
        var v = values[0]
        values.forEach {
            if $0.id == id {
                v = $0
                return
            }
        }
        return v
//        switch id {
//        case 0:
//            return Experience.values[0]
//        case 1..<3:
//            return  Experience.values[1]
//        case 3..<6:
//            return Experience.values[2]
//        default:
//            return Experience.values[3]
//        }
    }
}
