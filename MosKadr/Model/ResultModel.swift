//
//  ResultModel.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import Foundation
import ObjectMapper

class ResulInfoModel: Mappable {
    var results: [ResultModel] = []
    var filter: FilterModel!
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        results    <- map["result"]
        filter     <- map["filter"]
    }
}

class ResultModel: Mappable, Age {
    var id: Int!
    var nationality: Int!
    var name: String!
    var profession: [String]!
    var age: Int!
    var experience: Int = 0
    var foto: String?
    var salary: Int = 0

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["id"]
        age    <- map["age"]
        name    <- map["name"]
        nationality <- map["nationality"]
        profession <- map["profession"]
        foto    <- map["foto"]
        salary <- map["salary"]
        experience <- map["experience"]
    }
}

protocol Age {
    var age: Int! { get set }
}

extension Age {
    var fomatterdAge: String {
        let years: String
        switch age % 10 {
        case 1:
            years = "год"
        case 2,3,4:
            years = "года"
        default:
            years = "лет"
        }
        return String(age!) + " " + years
    }
}

class FilterModel: Mappable {
    var page: Int = 0
    var page_count: Int = 0
    var page_on: Int = 35
    var profession_group: Int = 0
    var profession: [Int] = []
    var resume_total: Int = 0
    var nationality: Int?
    var sex: Int?
    var age_from: Int?
    var experience_from: Int?
    var experience_to: Int?

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        page    <- map["page"]
        page_count    <- map["page_count"]
        page_on    <- map["page_on"]
        profession_group <- map["profession_group"]
        profession <- map["profession"]
        resume_total <- map["resume_total"]
        nationality <- map["nationality"]
        sex <- map["sex"]
        age_from <- map["age_from"]
        experience_from <- map["experience_from"]
        experience_to <- map["experience_to"]

    }
}
