//
//  SignUpModel.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import Foundation

class SignUpModel: NSObject {
    var group: ProfessionGroupModel!
    var professions: [Int] = []
    var salary: Int = 0
    var birthDate: String!
    var name: String!
    var nationality: Int = 0
    var experience: ExperienceTuple!
    var isMan: Bool!
    var phone: String!
    var password: String = ""
    var email: String?
    var vkLink: String?
    var fbLink: String?
    var okLink: String?
    var image: UIImage?
    var about: String?
    var metro_id: Int?

    convenience init(with user: UserModel) {
        self.init()
        professions = user.profession
        salary = user.salary
        birthDate = user.birthday
        name = user.name
        nationality = user.nationality
        experience = Experience.value(for: user.experience)
        isMan = user.sex == 1
        metro_id = user.metro_id
        phone = user.phone
        email = user.email
        vkLink = user.vk_link
        fbLink = user.fb_link
        okLink = user.ok_link
        about = user.about
    }
    
    func getSignUpParameters() -> [String: String] {
        var parameters = ["type":"1"]
        parameters["salary"] = "\(salary)"
        parameters["nationality"] = "\(nationality)"
        parameters["name"] = name
        var professionsId: [String] = []
        for professionId in professions {
            professionsId.append("\(professionId)")
        }
        if let metro = metro_id {
            parameters["metro_id"] = String(metro)
        }
        parameters["profession"] = professionsId.joined(separator: "-")
        parameters["email"] = email ?? ""
        parameters["phone"] = phone
        parameters["sex"] = isMan == true ? "1" : "2"
        parameters["birthday"] = "01.01." + birthDate
        parameters["experience"] = "\(experience.id)"
        parameters["login"] = phone
        parameters["password"] = password
//        if image != nil {
//            parameters["photo"] = image!.convertImageToBase64()
//            parameters["photo_ext"] = "jpeg"
//        }
        if vkLink != nil {
            parameters["vk_link"] = vkLink!
        }
        if fbLink != nil {
            parameters["fb_link"] = fbLink!
        }
        if okLink != nil {
            parameters["ok_link"] = okLink!
        }
        if about != nil {
            parameters["about"] = about!
        }
        return parameters
    }
    
    func getUpdateParameters() -> [String: String] {
        var parameters = ["type":"1"]
        parameters["token"] = AppManager.shared.options.user!.token
        parameters["salary"] = "\(salary)"
        parameters["nationality"] = "\(nationality)"
        parameters["name"] = name
        if let metro = metro_id {
            parameters["metro_id"] = String(metro)
        }
        var professionsId: [String] = []
        for professionId in professions {
            professionsId.append("\(professionId)")
        }
        parameters["profession"] = professionsId.joined(separator: "-")
        parameters["email"] = email ?? ""
        parameters["phone"] = phone
        parameters["sex"] = isMan == true ? "1" : "2"
        parameters["birthday"] = "01.01." + birthDate
        parameters["experience"] = "\(experience.id)"
        parameters["login"] = phone
//        if image != nil {
//            parameters["photo"] = image!.convertImageToBase64()
//            parameters["photo_ext"] = "jpeg"
//        }
        if vkLink != nil {
            parameters["vk_link"] = vkLink!
        }
        if fbLink != nil {
            parameters["fb_link"] = fbLink!
        }
        if okLink != nil {
            parameters["ok_link"] = okLink!
        }
        if about != nil {
            parameters["about"] = about!
        }
        return parameters
    }

}
