//
//  UserModel.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import ObjectMapper

class UserModel: Mappable, Age {
    var id: Int!
    var metro_id: Int?
    var age: Int!
    var resumeStatus = 1
    var date: String = ""
    var type: Int! = 0 // 0 - employer, 1 - worker
    var token: String = ""
    var name: String!
    var verified: Bool!
    var nationality: Int!
    var profession: [Int] = []
    var email: String!
    var experience: Int!
    var phone: String!
    var foto: String = ""
    var sex: Int = 1 // 1 - male, 2 - female
    var birthday: String!
    var salary: Int = 0
    var vk_link: String = ""
    var fb_link: String = ""
    var ok_link: String = ""
    var about: String = ""

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id    <- map["info.id"]
        metro_id    <- map["info.metro_id"]
        age    <- map["info.age"]
        type    <- map["info.resume_status"]
        type    <- map["info.type"]
        token    <- map["info.token"]
        name    <- map["info.name"]
        date    <- map["info.date"]
        verified <- map["info.verified"]
        nationality <- map["info.nationality"]
        profession <- map["info.profession"]
        email    <- map["info.email"]
        phone    <- map["info.phone"]
        foto    <- map["info.foto"]
        sex    <- map["info.sex"]
        birthday <- map["info.birthday"]
        salary <- map["info.salary"]
        vk_link <- map["info.vk_link"]
        fb_link <- map["info.fb_link"]
        ok_link <- map["info.ok_link"]
        experience <- map["info.experience"]
        about <- map["info.about"]
        resumeStatus <- map["info.resume_status"]
    }
    
    func groups() -> [ProfessionGroupModel] {
        var groups: [ProfessionGroupModel] = []
        for group in AppManager.shared.options.profession_group {
            let tempGroup = ProfessionGroupModel.init()
            tempGroup.id = group.id
            tempGroup.name = group.name
            tempGroup.profession = []
            for profession in group.profession {
                for id in self.profession {
                    if profession.id == id {
                        tempGroup.profession.append(profession)
                    }
                }
            }
            if tempGroup.profession.count > 0 {
                groups.append(tempGroup)
            }
        }
        return groups
    }
}

class MessageModel: Mappable {
    var title: String = "Сообщение"
    var text: String = "Сообщение"

    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        title    <- map["title"]
        text    <- map["text"]
    }    
}
