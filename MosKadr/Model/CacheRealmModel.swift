//
//  CacheRealmModel.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import Foundation
import RealmSwift

class CacheRealmModel: Object{
    dynamic var parameters: String? = nil
    dynamic var url: String = ""
    dynamic var seconds: Int = 0
    dynamic var date: Date = Date()
    dynamic var response: String = ""
    
    convenience init(url: String, response: Dictionary<String, Any>, seconds: Int, parameters: Dictionary<String, String>? = nil) {
        self.init()
        self.url = url
        if let responseString = response.convertToString() {
            self.response = responseString
        }
        if parameters != nil, let parametersString = parameters!.convertToString() {
            self.parameters = parametersString
        }
        self.seconds = seconds
        self.date = Date()
    }
    
    
    /**
     Get expiry date.
     - returns: expiry date for cache
     */
    func expiryDate() -> Date {
        return date.addingTimeInterval(TimeInterval(seconds))
    }
}
