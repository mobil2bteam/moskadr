//
//  OptionsModel.swift
//  MosKadr
//
//  Created by Ruslan on 10/16/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import ObjectMapper

class OptionsModel: Mappable {
    var cache: Int?
    var urls: [UrlModel] = []
    var nationality: [NationalityModel] = []
    var profession_group: [ProfessionGroupModel] = []
    var metro: [MetroModel] = []
    var user: UserModel?
    var resumeCount: Int = 0
    
    required init?(map: Map) {
        
    }
    
    func nationality(for id: Int) -> NationalityModel? {
        for n in nationality {
            if n.id == id {
                return n
            }
        }
        return nil
    }
    
    // Mappable
    func mapping(map: Map) {
        cache    <- map["cache"]
        urls     <- map["options.urls"]
        nationality     <- map["directory.nationality"]
        metro     <- map["directory.metro"]
        profession_group <- map["directory.profession_group"]
        user <- map["user"]
        resumeCount <- map["directory.resume_count"]
    }
    
    func urlFor(name: String) -> UrlModel? {
        for url in urls {
            if url.name == name {
                return url
            }
        }
        return nil
    }
}

class UrlModel: Mappable {
    var name: String?
    var url: String?
    var method: String?
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        name    <- map["name"]
        url    <- map["url"]
        method    <- map["method"]
    }
}

class NationalityModel: Mappable {
    var id: Int = 0
    var name: String!
    var image: String?
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        name    <- map["name"]
        image    <- map["image"]
    }
}

class ProfessionGroupModel: NSObject, Mappable {
    var id: Int = 0
    var name: String!
    var profession: [ProfessionModel] = []

    required init?(map: Map) {
        
    }
    
    override init () {
    }
    
    func getAllProfessionsText(separator: String = ",") -> String {
        var names: [String] = []
        for p in profession {
            names.append(p.name)
        }
        return names.joined(separator: separator)
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        name    <- map["name"]
        profession    <- map["profession"]
    }
}

class MetroModel: Mappable {
    var id: Int = 0
    var name: String!
    var line: String!
    var color: String!
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        name    <- map["name"]
        line    <- map["line"]
        color    <- map["color"]
    }
}


class ProfessionModel: Mappable {
    var id: Int = 0
    var name: String!
    
    required init?(map: Map) {
        
    }
    
    // Mappable
    func mapping(map: Map) {
        id    <- map["id"]
        name    <- map["name"]
    }
}
