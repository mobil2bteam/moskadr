//
//  RPNavControllerDelegate.h
//  ProObed
//
//  Created by Ruslan on 12/29/16.
//  Copyright © 2016 Ruslan Palapa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface RPNavControllerDelegate : NSObject <UINavigationControllerDelegate>

@end
