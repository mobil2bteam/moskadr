//
//  AppManager.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class AppManager: NSObject {
    
    static let shared: AppManager = {
        return AppManager()
    }()
    
    var options: OptionsModel!
}
