//
//  APIManager.swift
//  MosKadr
//
//  Created by Ruslan on 10/16/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Alamofire
import ObjectMapper

class APIManager: NSObject {
    
    static let appKey = "000000"
    static let baseURL = "http://xn-----6kcbbdegrp4bbgjj6abm1bfdd1cyu.xn--p1ai/index.php?route=information%2Fwebapi"
    
    class func getOptions (successHandler: @escaping () -> Void, errorHandler: @escaping (Error) -> Void) {
        var parameters: [String: String] = [:]
        if let token = UserDefaults.standard.string(forKey: "token") {
            parameters["token"] = token
        }
//        Alamofire.SessionManager.default.session.configuration.timeoutIntervalForRequest = 120
//        if let existCache = CacheManager.cache(for: baseURL, parameters: parameters) {
//            let options = Mapper<OptionsModel>().map(JSON: existCache.response.convertToDictionary()!)
//            AppManager.shared.options = options
//            successHandler()
//            return
//        }
        let url = URL(string: baseURL)
        
        Alamofire.request(url!, method: .post, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                print(responseJSON!)
                let options = Mapper<OptionsModel>().map(JSON: responseJSON!)
                AppManager.shared.options = options
                // Cache options
                if let cacheTime = options?.cache {
                    let cacheResponse = CacheRealmModel(url: baseURL, response: responseJSON!, seconds: cacheTime, parameters: parameters)
                    CacheManager.addCache(cache: cacheResponse)
                }
                successHandler()
        }
    }
    
    class func signIn (login: String, password: String, type: Int, successHandler: @escaping () -> Void, errorHandler: @escaping (String) -> Void) {
        let parameters: [String: String] = ["login":login, "password":password, "type":"\(type)"]
        let url = AppManager.shared.options.urlFor(name: "url_user_login")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                print(responseJSON ?? "")
                guard let userDictionary = responseJSON!["user"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let user = Mapper<UserModel>().map(JSON: userDictionary)
                AppManager.shared.options.user = user
                // save token
                UserDefaults.standard.set(user!.token, forKey: "token")
                UserDefaults.standard.synchronize()
                successHandler()
        }
    }
    
    class func signUp (parameters: [String: String], image: UIImage?, successHandler: @escaping () -> Void, errorHandler: @escaping (String) -> Void) {
        
        let url = AppManager.shared.options.urlFor(name: "url_user_registration")
        let urlString = validUrlFrom(url: (url?.url)!)

        var data: Data? = nil
        if image != nil {
            if image!.width > 1500 {
                let size = CGSize(width: 1500, height: image!.height * (1500.0 / image!.width))
                let resizedImage = image!.resizeImage(to: size)
                data = UIImageJPEGRepresentation(resizedImage, 1)
            } else {
                data = UIImageJPEGRepresentation(image!, 1)
            }
        }

        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let imageData = data {
                multipartFormData.append(imageData, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
            }
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseString { response in
                    
                }
                upload.responseJSON { response in
                    if let error = response.error {
                        errorHandler(error.localizedDescription)
                    } else {
                        let responseJSON = response.result.value as? [String: Any]
                        guard let userDictionary = responseJSON!["user"] as? [String: Any] else {
                            let errorJSON = responseJSON!["error"] as? [String: Any]
                            let message = errorJSON!["text"] as! String
                            errorHandler(message)
                            return
                        }
                        
                        let user = Mapper<UserModel>().map(JSON: userDictionary)
                        AppManager.shared.options.user = user
                        // save token
                        UserDefaults.standard.set(user!.token, forKey: "token")
                        UserDefaults.standard.synchronize()
                        successHandler()
                    }
                }
            case .failure(let error):
                errorHandler(error.localizedDescription)
            }
        }
    }
    
    class func signUpEmployer (parameters: [String: String], successHandler: @escaping () -> Void, errorHandler: @escaping (String) -> Void) {
        
        let url = AppManager.shared.options.urlFor(name: "url_user_registration")
        let urlString = validUrlFrom(url: (url?.url)!)
        
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                print(responseJSON ?? "")
                guard let userDictionary = responseJSON!["user"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let user = Mapper<UserModel>().map(JSON: userDictionary)
                AppManager.shared.options.user = user
                // save token
                UserDefaults.standard.set(user!.token, forKey: "token")
                UserDefaults.standard.synchronize()
                successHandler()
        }

    }

    class func updateUser (parameters: [String: String], image: UIImage?, successHandler: @escaping () -> Void, errorHandler: @escaping (String) -> Void) {
        let url = AppManager.shared.options.urlFor(name: "url_user_update")
        let urlString = validUrlFrom(url: (url?.url)!)
        
        var data: Data? = nil
        if image != nil {
            if image!.width > 1500 {
                let size = CGSize(width: 1500, height: image!.height * (1500.0 / image!.width))
                let resizedImage = image!.resizeImage(to: size)
                data = UIImageJPEGRepresentation(resizedImage, 1)
            } else {
                data = UIImageJPEGRepresentation(image!, 1)
            }
        }
        
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if let imageData = data {
                multipartFormData.append(imageData, withName: "photo", fileName: "image.jpg", mimeType: "image/jpeg")
            }
        }, usingThreshold: UInt64.init(), to: urlString, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseString { response in
                    
                }
                upload.responseJSON { response in
                    if let error = response.error {
                        errorHandler(error.localizedDescription)
                    } else {
                        let responseJSON = response.result.value as? [String: Any]                        
                        guard let userDictionary = responseJSON!["user"] as? [String: Any] else {
                            let errorJSON = responseJSON!["error"] as? [String: Any]
                            let message = errorJSON!["text"] as! String
                            errorHandler(message)
                            return
                        }
                        
                        let user = Mapper<UserModel>().map(JSON: userDictionary)
                        AppManager.shared.options.user = user
                        // save token
                        UserDefaults.standard.set(user!.token, forKey: "token")
                        UserDefaults.standard.synchronize()
                        successHandler()
                    }
                }
            case .failure(let error):
                errorHandler(error.localizedDescription)
            }
        }
    }
    
    
    class func userExist (with phone: String, successHandler: @escaping (_ exist: Bool) -> Void, errorHandler: @escaping (String) -> Void) {
        let parameters: [String: String] = ["phone":phone]
        let url = AppManager.shared.options.urlFor(name: "url_user_exist")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                if let existDictionary = responseJSON!["user_exist"] as? [String: Any],
                    let exist = existDictionary["success"] as? Bool {
                    successHandler(exist)
                } else {
                    successHandler(false)
                }
        }
    }
    
    class func updateResume(successHandler: @escaping (_ message: String) -> Void) {
        let token = AppManager.shared.options.user?.token
        var parameters: [String: String] = [:]
        if token != nil {
            parameters["token"] = token!
        }
        let url = AppManager.shared.options.urlFor(name: "url_update_resume")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    successHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                print(responseJSON ?? "")
                if let error = responseJSON!["error"] as? [String: Any],
                    let text = error["text"] as? String {
                    successHandler(text)
                } else if let message = responseJSON!["message"] as? [String: Any], let text = message["text"] as? String {
                    if let userDictionary = responseJSON!["user"] as? [String: Any] {
                        let user = Mapper<UserModel>().map(JSON: userDictionary)
                        AppManager.shared.options.user = user
                        // save token
                        UserDefaults.standard.set(user!.token, forKey: "token")
                        UserDefaults.standard.synchronize()
                    }
                    successHandler(text)
                } else {
                    successHandler("Произошла ошибка")
                }
        }
    }

    class func restorePassword (email: String, type: Int, successHandler: @escaping (_ message: MessageModel) -> Void, errorHandler: @escaping (String) -> Void) {
        let parameters: [String: String] = ["email":email, "type":"\(type)"]
        let url = AppManager.shared.options.urlFor(name: "url_user_password_memory")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard let messageDictionary = responseJSON!["message"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let message = Mapper<MessageModel>().map(JSON: messageDictionary)
                successHandler(message!)
        }
    }
    
    class func sendMessage (message: String, email: String, successHandler: @escaping (_ message: MessageModel) -> Void, errorHandler: @escaping (String) -> Void) {
        let parameters: [String: String] = ["message":message, "from": email]
        let url = AppManager.shared.options.urlFor(name: "url_send_error_message")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard let messageDictionary = responseJSON!["message"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let message = Mapper<MessageModel>().map(JSON: messageDictionary)
                successHandler(message!)
        }
    }

    class func changePassword (oldPassword: String, newPassword: String, token: String, successHandler: @escaping () -> Void, errorHandler: @escaping (String) -> Void) {
        let parameters: [String: String] = ["password_old":oldPassword, "password_new":newPassword, "token": token]
        let url = AppManager.shared.options.urlFor(name: "url_user_password_change")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard let messageData = responseJSON!["message"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let message = messageData["text"] as! String
                successHandler()
        }
    }
    
    class func getWorkers (with parameters: [String: String], successHandler: @escaping (_ result: ResulInfoModel) -> Void, errorHandler: @escaping (String) -> Void) {
        let url = AppManager.shared.options.urlFor(name: "url_search")
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard let resultDictionary = responseJSON!["search_result"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let result = Mapper<ResulInfoModel>().map(JSON: resultDictionary)
                successHandler(result!)
        }
    }
    
    class func getResume (with id: Int, successHandler: @escaping (_ result: ResumeModel) -> Void, errorHandler: @escaping (String) -> Void) {
        let url = AppManager.shared.options.urlFor(name: "url_resume")
        let token = AppManager.shared.options.user?.token
        var parameters: [String: String] = ["id": "\(id)"]
        if token != nil {
            parameters["token"] = token!
        }
        let urlString = validUrlFrom(url: (url?.url)!)
        let method = HTTPMethod(rawValue: (url?.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                print(responseJSON ?? "")
                guard let dic = responseJSON!["resume_info"] as? [String: Any] else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                let resume = Mapper<ResumeModel>().map(JSON: dic)
                successHandler(resume!)
        }
    }
    
    class func enableResume (_ enable: Bool, successHandler: @escaping (_ text: String) -> Void, errorHandler: @escaping (String) -> Void) {
        var urlModel: UrlModel?
        if enable == true {
            urlModel = AppManager.shared.options.urlFor(name: "url_enable_resume")
        } else {
            urlModel = AppManager.shared.options.urlFor(name: "url_disable_resume")
        }
        guard let url = urlModel else {
            errorHandler("Произошла ошибка")
            return
        }
        let parameters: [String: String] = ["token": AppManager.shared.options.user!.token]
        let urlString = validUrlFrom(url: (url.url)!)
        let method = HTTPMethod(rawValue: (url.method?.uppercased())!)
        Alamofire.request(urlString, method: method!, parameters: parameters)
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    errorHandler(response.error!.localizedDescription)
                    return
                }
                let responseJSON = response.result.value as? [String: Any]
                guard let dic = responseJSON!["message"] as? [String: Any], let text = dic["text"] as? String else {
                    let errorJSON = responseJSON!["error"] as? [String: Any]
                    let message = errorJSON!["text"] as! String
                    errorHandler(message)
                    return
                }
                successHandler(text)
        }
    }

    
    class func validUrlFrom(url: String) -> String {
        return url.replacingOccurrences(of: "http://московская-служба-кадров.рф", with: "http://xn-----6kcbbdegrp4bbgjj6abm1bfdd1cyu.xn--p1ai")
    }
}
