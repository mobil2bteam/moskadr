import UIKit

class MetroListViewController: UIViewController {
    @IBOutlet weak var metroTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    var completion: ((MetroModel) -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        metroTableView.register(UINib.init(nibName: "MetroTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    }

    func metroArray() -> [MetroModel] {
        let searchText = searchBar.text
        if (searchText!.count > 0) {
            return AppManager.shared.options.metro.filter {
                $0.name.range(of: searchText!, options: .caseInsensitive) != nil
            }
        } else {
            return AppManager.shared.options.metro
        }
    }
    
    @IBAction func dismissClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension MetroListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return metroArray().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MetroTableViewCell
        cell.nameLabel.text = metroArray()[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let temp = metroArray()[indexPath.row]
        completion?(temp)
        dismiss(animated: true, completion: nil)
    }
}

extension MetroListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        DispatchQueue.global().asyncAfter(deadline: .now() + 0.3) {
            DispatchQueue.main.async {
                self.metroTableView.reloadData()
            }
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
}

