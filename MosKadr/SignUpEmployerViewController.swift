import UIKit
import SVProgressHUD

class SignUpEmployerViewController: UIViewController {
    @IBOutlet weak var companyNameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var siteTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!

    var completion: (() -> ())?

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Регистрация работодателя"
    }
    
    @IBAction func signUpButtonClicked(_ sender: Any) {
        if companyNameTextField.text!.isEmpty {
            companyNameTextField.shake()
            return
        }
        if phoneTextField.text!.isEmpty {
            phoneTextField.shake()
            return
        }
        if emailTextField.text!.isEmpty {
            emailTextField.shake()
            return
        }
        if passwordTextField.text!.isEmpty {
            passwordTextField.shake()
            return
        }
        let company = companyNameTextField.text!
        let phone = phoneTextField.text!
        let email = emailTextField.text!
        let password = passwordTextField.text!
        let site = siteTextField.text!
        
        var params = ["company": company,
                      "type": "0",
                      "login": email,
                      "password": password,
                      "phone": phone,
                      "email": email]
        if !site.isEmpty {
            params["site_url"] = site
        }
        SVProgressHUD.show()
        APIManager.signUpEmployer(parameters: params, successHandler: {
            SVProgressHUD.dismiss()
            self.completion?()
            self.dismiss(animated: true, completion: nil)
        }) { [weak self] (error) in
            guard let `self` = self else { return }
            SVProgressHUD.dismiss()
            self.alert(message: error)
        }
    }
}

extension SignUpEmployerViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == companyNameTextField {
            phoneTextField.becomeFirstResponder()
        }
        if textField == phoneTextField {
            emailTextField.becomeFirstResponder()
        }
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        }
        if textField == passwordTextField {
            siteTextField.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }
}
