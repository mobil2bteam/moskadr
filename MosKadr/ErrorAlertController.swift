import UIKit
import Material
import SVProgressHUD

class ErrorAlertController: UIViewController {

    @IBOutlet weak var messageTextField: ErrorTextField!
    @IBOutlet weak var emailTextField: ErrorTextField!
    
    //MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        _ = messageTextField.becomeFirstResponder()
    }
    
    //MARK: Actions
    @IBAction func sendButtonPressed(_ sender: Any) {
        view.endEditing(true)
        messageTextField.isErrorRevealed = messageTextField.text!.count == 0
        if (messageTextField.text!.count == 0) {
            messageTextField.shake()
            return
        }
        emailTextField.isErrorRevealed = emailTextField.text!.count == 0
        if (emailTextField.text!.count == 0) {
            emailTextField.shake()
            return
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.sendMessage(message: messageTextField.text!, email: emailTextField.text!, successHandler: { (message) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: message.text, title: message.title, handler: { (action) in
                weakSelf?.dismiss(animated: true, completion: nil)
            })
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension ErrorAlertController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == messageTextField {
            _ = emailTextField.becomeFirstResponder()
        }
        return true
    }
}
