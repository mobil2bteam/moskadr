//
//  AppDelegate.swift
//  MosKadr
//
//  Created by Ruslan on 10/16/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SVProgressHUD
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        CacheManager.updateCache()
        Fabric.with([Crashlytics.self])
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.blueButtonBackgroundColor()
        IQKeyboardManager.sharedManager().enable = true
        prepareProgressHUD()
        prepareWindow()
        return true
    }

    func prepareWindow() {
        let startViewController = StartViewController.init()
        let navVC = RPTransitionNavController.init(rootViewController: startViewController)
        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.backgroundColor = UIColor.white
        window?.rootViewController = navVC
        window?.makeKeyAndVisible()
    }
    
    func prepareProgressHUD() {
        SVProgressHUD.setDefaultStyle(.dark)
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.setMinimumDismissTimeInterval(1)
        SVProgressHUD.setMaximumDismissTimeInterval(1)
    }

}


