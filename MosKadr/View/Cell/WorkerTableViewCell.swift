//
//  WorkerTableViewCell.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import AlamofireImage

class WorkerTableViewCell: UITableViewCell {

    @IBOutlet weak var workerImageView: UIImageView!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var professionLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    
    func configure(for result: ResultModel) {
        salaryLabel.text = "\(result.salary) руб."
        ageLabel.text = result.fomatterdAge
        professionLabel.text = result.profession.joined(separator: ", ")
        if result.experience != 0 {
            experienceLabel.text = "Опыт работы: \(Experience.value(for: result.experience).value)"
        }
        for n in AppManager.shared.options.nationality {
            if n.id == result.nationality && n.image != nil {
                if let url = URL(string: n.image!) {
                    flagImageView.af_setImage(withURL: url)
                }
            }
        }
        guard let image = result.foto,
            let url =  URL(string: image) else {
                return
        }
        workerImageView.af_setImage(withURL: url)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        workerImageView.image = UIImage.init(named: "placeholder.png")
        flagImageView.image = nil
        experienceLabel.text = "Опыт работы: Без опыта"
    }

}
