//
//  ProfessionTableViewCell.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class ProfessionTableViewCell: UITableViewCell {

    @IBOutlet weak var checkmarkView: UIView!
    @IBOutlet weak var professionLabel: UILabel!

    func configure(for profession: ProfessionModel) {
        professionLabel.text = profession.name
    }
    
    func setCheckmark(checked: Bool) {
        if checked {
            checkmarkView.layer.borderWidth = 0
            checkmarkView.backgroundColor = UIColor.blueButtonBackgroundColor()
        } else {
            checkmarkView.layer.borderWidth = 1
            checkmarkView.backgroundColor = UIColor.white
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        professionLabel.text = ""
    }
}
