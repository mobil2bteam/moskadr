//
//  GroupEditTableViewCell.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class GroupEditTableViewCell: UITableViewCell {

    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var professionsLabel: UILabel!
    
    func configure(for group: ProfessionGroupModel) {
        groupLabel.text = group.name
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        groupLabel.text = ""
        professionsLabel.text = ""
    }

}
