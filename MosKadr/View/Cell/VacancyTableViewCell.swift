//
//  VacancyTableViewCell.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class VacancyTableViewCell: UITableViewCell {
    @IBOutlet weak var groupLabel: UILabel!
    @IBOutlet weak var professionsLabel: UILabel!
}
