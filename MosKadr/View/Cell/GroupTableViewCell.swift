//
//  GroupTableViewCell.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class GroupTableViewCell: UITableViewCell {

    @IBOutlet weak var groupLabel: UILabel!
    
    func configure(for group: ProfessionGroupModel) {
        groupLabel.text = group.name
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        groupLabel.text = ""
    }
}
