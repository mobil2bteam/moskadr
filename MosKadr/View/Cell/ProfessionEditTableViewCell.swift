//
//  ProfessionEditTableViewCell.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class ProfessionEditTableViewCell: UITableViewCell {

    @IBOutlet weak var checkmarkView: UIView!
    @IBOutlet weak var checkmarkImageView: UIImageView!
    @IBOutlet weak var professionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setCheckmark(checked: Bool) {
        checkmarkImageView.isHidden = !checked
        if checked {
            checkmarkView.layer.borderWidth = 0
            checkmarkView.backgroundColor = UIColor.blueButtonBackgroundColor()
        } else {
            checkmarkView.layer.borderWidth = 1
            checkmarkView.backgroundColor = UIColor.white
        }
    }

}
