//
//  SecondButton.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class SecondButton: UIButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        setBackgroundImage(UIImage.init(color: UIColor.blueButtonBackgroundColor()), for: .normal)
        setBackgroundImage(UIImage.init(color: UIColor.blueButtonBackgroundColorHighlighted()), for: .highlighted)
    }
    
}
