//
//  MainButton.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import RNLoadingButton_Swift

class MainButton: RNLoadingButton {

    override func awakeFromNib() {
        super.awakeFromNib()
        setBackgroundImage(UIImage.init(color: UIColor.redButtonBackgroundColor()), for: .normal)
        setBackgroundImage(UIImage.init(color: UIColor.redButtonBackgroundColorHighlighted()), for: .highlighted)
    }
    
}
