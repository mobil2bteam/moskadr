//
//  CustomTextField.swift
//  MosKadr
//
//  Created by Ruslan on 10/19/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import Foundation
import Material

class CustomTextField: ErrorTextField {
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return CGRect.zero
    }
    
    override func selectionRects(for range: UITextRange) -> [Any] {
        return []
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        if action == #selector(copy(_:)) || action == #selector(selectAll(_:)) || action == #selector(paste(_:)) {
            return false
        }
        
        return super.canPerformAction(action, withSender: sender)
    }
}
