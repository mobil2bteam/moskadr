//
//  PhotoViewController.swift
//  MosKadr
//
//  Created by Ruslan on 22.02.2018.
//  Copyright © 2018 Renat Ganiev. All rights reserved.
//

import UIKit
import AlamofireImage

class PhotoFullScreeViewController: UIViewController {

    var imageUrl: String!
    
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: Lifecycle
    convenience init(imageUrl: String) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.imageUrl = imageUrl
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureImageView()
        configureNavigationBar()
    }
    
    // MARK: Methods
    func configureNavigationBar() {
        let closeButton = UIBarButtonItem.init(barButtonSystemItem: .done, target: self, action: #selector(self.closeButtonAction))
        navigationItem.rightBarButtonItem = closeButton
        navigationController?.navigationBar.barTintColor = UIColor.black
        navigationController?.navigationBar.tintColor = UIColor.white
    }
    
    func configureImageView() {
        let photoUrl = URL(string: imageUrl)
        if photoUrl != nil {
            imageView.af_setImage(withURL: photoUrl!)
        }
    }
    
    // MARK: Actions
    func closeButtonAction(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }

}
