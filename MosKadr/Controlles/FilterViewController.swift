//
//  FilterViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/23/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Material
import AlamofireImage

typealias FilterCompletionBlock = ([String: String]) -> Void

class FilterViewController: UIViewController {
    var resultInfo: ResulInfoModel!
    var professions: [Int] = []
    var filterBlock: FilterCompletionBlock = {_ in }
    var groups = AppManager.shared.options.profession_group
    var nationality: NationalityModel? {
        didSet {
            nationalityTextField.text = nationality?.name
            guard let image = nationality?.image,
                let url =  URL(string: image) else {
                    flagImageView.image = UIImage.init(named: "flag_placeholder")
                    return
            }
            flagImageView.af_setImage(withURL: url)
        }
    }
    var age: String?
    var experience: ExperienceTuple?
    var currentGroup: ProfessionGroupModel!
    
    lazy var ageValues: [String] = {
        return ["14-18", "18-22", "22-30", "30-40", "40-50", "50-60", "60-70"]
    }()

    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var groupTextField: ErrorTextField!
    @IBOutlet weak var nationalityTextField: CustomTextField!
    @IBOutlet weak var professionsTextField: CustomTextField!
    @IBOutlet weak var experienceTextField: CustomTextField!
    @IBOutlet weak var ageTextField: CustomTextField!
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    
    lazy var nationalityPicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    lazy var experiencePicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()

    lazy var agePicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()

    lazy var groupPicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()

    //MARK: Lifecycle
    convenience init(result: ResulInfoModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.resultInfo = result
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureAllViews()
        prepareFilterValues()
        // add close bar button
        let rightBarButton = UIBarButtonItem.init(title: "Применить", style: .done, target: self, action: #selector(self.rightButtonAction))
        navigationItem.rightBarButtonItem = rightBarButton

    }
    
    func prepareFilterValues() {
        // set group and professions
        for group in groups {
            if group.id == resultInfo.filter.profession_group {
                currentGroup = group
                groupTextField.text = group.name
                professions = resultInfo.filter.profession
                professionsTextField.text = professionText()
                break
            }
        }
        // set nationality if needed
        if let nationalityId = resultInfo.filter.nationality {
            for n in AppManager.shared.options.nationality {
                if n.id == nationalityId {
                    nationality = n
                }
            }
        }
        // set gender if needed
        if let gender = resultInfo.filter.sex {
            if gender == 0 {
                genderSegmentControl.selectedSegmentIndex = 2
            }
            if gender == 1 {
                genderSegmentControl.selectedSegmentIndex = 0
            }
            if gender == 2 {
                genderSegmentControl.selectedSegmentIndex = 1
            }
        }
        // set age if needed
        if let ageFrom = resultInfo.filter.age_from {
            for age in ageValues {
                if Int(age.components(separatedBy: "-")[0]) == ageFrom {
                    self.age = age
                    ageTextField.text = "\(self.age!) лет"
                }
            }
        }
        // set experience if needed
        if let experienceFrom = resultInfo.filter.experience_from {
            experience = Experience.value(for: experienceFrom)
            experienceTextField.text = experience?.value
        }
    }
    
    //MARK: Actions
    func rightButtonAction(sender: UIBarButtonItem) {
        var parameters = ["profession_group":"\(currentGroup.id)"]
        if let token = AppManager.shared.options.user?.token {
            parameters["token"] = token
        }
        if age != nil {
            parameters["age_from"] = age?.components(separatedBy: "-")[0]
            parameters["age_to"] = age?.components(separatedBy: "-")[1]
        }
        if nationality != nil {
            parameters["nationality"] = "\(nationality!.id)"
        }
        if professions.count > 0 {
            var temp = [String]()
            for p in professions {
                temp.append("\(p)")
            }
            parameters["profession"] = temp.joined(separator: "-")
        }
        if experience != nil {
            parameters["experience_from"] = String(experience!.id)
//            parameters["experience_to"] = experience!.experience_to
        }
        switch genderSegmentControl.selectedSegmentIndex {
        case 0:
            parameters["sex"] = "1"
        case 1:
            parameters["sex"] = "2"
        default:
            parameters["sex"] = "0"
        }
        filterBlock(parameters)
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func professionButtonPressed(_ sender: Any) {
        weak var weakSelf = self
        let alert = ProfessionsAlertViewController.init(professions: professions, group: currentGroup)
        alert.completionHandler = { professions in
            weakSelf?.professions = professions
            weakSelf?.professionsTextField.text = weakSelf?.professionText()
        }
        let navVC = UINavigationController.init(rootViewController: alert)
        present(navVC, animated: true, completion: nil)
    }
    
    //MARK: Methods
    func configureAllViews() {
        nationalityTextField.inputView = nationalityPicker
        experienceTextField.inputView = experiencePicker
        ageTextField.inputView = agePicker
        groupTextField.inputView = groupPicker
        for picker in [nationalityPicker, agePicker, groupPicker, experiencePicker] {
            picker.dataSource = self
            picker.delegate = self
        }
    }
    
    // MARK: Helpers
    fileprivate func professionText() -> String {
        var names: [String] = []
        for p in currentGroup.profession {
            for id in professions {
                if p.id == id {
                    names.append(p.name)
                }
            }
        }
        return names.joined(separator: ", ")
    }
}


extension FilterViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if pickerView == nationalityPicker {
            let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 44))
            let myImageView = UIImageView(frame: CGRect(x: pickerView.bounds.width - 60, y: 0, width: 30, height: 44))
            let myLabel = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 60, height: 44))
            myLabel.textAlignment = .center
            myView.addSubview(myImageView)
            myView.addSubview(myLabel)
            myView.translatesAutoresizingMaskIntoConstraints = true
            myImageView.contentMode = .scaleAspectFit
            
            let nationality = AppManager.shared.options.nationality[row]
            myLabel.text = nationality.name
            guard let image = nationality.image,
                let url =  URL(string: image) else {
                    myImageView.image = UIImage.init(named: "flag_placeholder")
                    return myView
            }
            myImageView.af_setImage(withURL: url)
            return myView
        }
        let myLabel = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 44))
        myLabel.textAlignment = .center
        if pickerView == experiencePicker {
            myLabel.text = Experience.values[row].value
            return myLabel
        }
        if pickerView == agePicker {
            myLabel.text = "\(ageValues[row]) лет"
            return myLabel
        }
        if pickerView == groupPicker {
            myLabel.text = groups[row].name
            return myLabel
        }
        return UIView.init()
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == nationalityPicker {
            return AppManager.shared.options.nationality.count
        }
        if pickerView == experiencePicker {
            return Experience.values.count
        }
        if pickerView == agePicker {
            return ageValues.count
        }
        if pickerView == groupPicker {
            return groups.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == agePicker {
            ageTextField.text = "\(ageValues[row]) лет"
            age = ageValues[row]
        }
        if pickerView == groupPicker {
            groupTextField.text = groups[row].name
            currentGroup = groups[row]
            professionsTextField.text = " "
            professions = []
        }
        if pickerView == experiencePicker {
            experienceTextField.text = Experience.values[row].value
            experience = Experience.values[row]
        }
        if pickerView == nationalityPicker {
            let nationality = AppManager.shared.options.nationality[row]
            self.nationality = nationality
        }
    }
}

