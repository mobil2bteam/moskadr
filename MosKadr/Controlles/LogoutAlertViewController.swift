//
//  LogoutAlertViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class LogoutAlertViewController: UIViewController {
    
    //MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    //MARK: Actions
    @IBAction func exitButtonPressed(_ sender: Any) {
        let startViewController = StartViewController.init()
        UserDefaults.standard.removeObject(forKey: "token")
        UserDefaults.standard.synchronize()
        let navVC = RPTransitionNavController.init(rootViewController: startViewController)
        UIApplication.shared.delegate?.window!?.rootViewController = navVC
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
