//
//  SocialLinksViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class SocialLinksViewController: UIViewController {
    var signUpModel: SignUpModel!
    var isEditingUserMode = false
    @IBOutlet weak var vkTextField: UITextField!
    @IBOutlet weak var fbTextField: UITextField!
    @IBOutlet weak var okTextField: UITextField!
    @IBOutlet weak var statusLabel: UILabel!

    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vkTextField.text = signUpModel.vkLink ?? ""
        okTextField.text = signUpModel.okLink ?? ""
        fbTextField.text = signUpModel.fbLink ?? ""
        
        if isEditingUserMode {
            statusLabel.text = "РЕДАКТИРОВАНИЕ"
            vkTextField.text = signUpModel.vkLink
            okTextField.text = signUpModel.okLink
            fbTextField.text = signUpModel.fbLink
        } else {
            let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        }
    }
    
    //MARK: Actions
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        signUpModel.vkLink = ""
        signUpModel.okLink = ""
        signUpModel.fbLink = ""
        if let text = vkTextField.text {
            if text.count > 0 {
                signUpModel.vkLink = text
            }
        }
        if let text = fbTextField.text {
            if text.count > 0 {
                signUpModel.fbLink = text
            }
        }
        if let text = okTextField.text {
            if text.count > 0 {
                signUpModel.okLink = text
            }
        }
        if isEditingUserMode {
            let salaryViewController = SalaryViewController.init(signUpModel: signUpModel)
            salaryViewController.isEditingUserMode = isEditingUserMode
            navigationController?.pushViewController(salaryViewController, animated: true)
        } else {
            let photoViewController = PhotoViewController.init(signUpModel: signUpModel)
            navigationController?.pushViewController(photoViewController, animated: true)
        }
    }
}

extension SocialLinksViewController: UITextFieldDelegate {
   
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fbTextField {
            okTextField.becomeFirstResponder()
        }
        if textField == vkTextField {
            fbTextField.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }
    
}
