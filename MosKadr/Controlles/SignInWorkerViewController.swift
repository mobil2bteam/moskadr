//
//  SignInWorkerViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/16/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD
import Material
import RNLoadingButton_Swift

class SignInWorkerViewController: UIViewController {
    @IBOutlet weak var phoneTextField: ErrorTextField!
    @IBOutlet weak var phoneView: UIView!
    @IBOutlet weak var nextButton: RNLoadingButton!
    
    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.detailVerticalOffset = 5
        phoneTextField.placeholderVerticalOffset = 24
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: Actions
    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        phoneTextField.isErrorRevealed = phoneTextField.text!.count < 10
        if (phoneTextField.text!.count < 10) {
            phoneView.shake()
            return
        }
        if let phone = phoneTextField.text {
            weak var weakSelf = self
            nextButton.isLoading = true
            APIManager.userExist(with: phone,successHandler: { (exist) in
                weakSelf?.nextButton.isLoading = false
                if exist {
                    weakSelf?.showLogInViewController(phone: phone)
                } else {
                    weakSelf?.showGroupsViewController(phone: phone)
                }
            }, errorHandler: { (error) in
                weakSelf?.nextButton.isLoading = false
                weakSelf?.alert(message: error)
            })
        }
    }
    
    // MARK: Methods
    func showLogInViewController(phone: String) {
        let logInViewController = LogInViewController.init(phone: phone)
        navigationController?.pushViewController(logInViewController, animated: true)
    }
    
    func showGroupsViewController(phone: String) {
        let groupsViewController = GroupsViewController.init()
        groupsViewController.phone = phone
        navigationController?.pushViewController(groupsViewController, animated: true)
    }

}

extension SignInWorkerViewController: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
    
}
