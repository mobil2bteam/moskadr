//
//  SignUpFinishViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class SignUpFinishViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //MARK: Methods
    @IBAction func nextButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(ProfileViewController.init(), animated: true)
    }

}
