
//
//  AboutViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/30/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class AboutViewController: UIViewController {
    var isEditingUserMode = false
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet weak var statusLabel: UILabel!
    var signUpModel: SignUpModel!

    
    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let a = signUpModel.about {
            aboutTextView.text = a
        }
        if isEditingUserMode {
            statusLabel.text = "РЕДАКТИРОВАНИЕ"
            aboutTextView.text = signUpModel.about ?? ""
        } else {
            let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        }
    }
    
    //MARK: Actions
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        signUpModel.about = aboutTextView.text
        if isEditingUserMode {
            let salaryViewController = SalaryViewController.init(signUpModel: signUpModel)
            salaryViewController.isEditingUserMode = isEditingUserMode
            navigationController?.pushViewController(salaryViewController, animated: true)
        } else {
            let photoViewController = PhotoViewController.init(signUpModel: signUpModel)
            navigationController?.pushViewController(photoViewController, animated: true)
        }
    }
}

extension AboutViewController: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text != "" && textView.text?.count == 500 {
            return false
        }
        return true
    }
}
