//
//  StartViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/16/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD

class StartViewController: UIViewController {
    
    @IBOutlet weak var searchingView: UIStackView!
    @IBOutlet weak var employerView: UIView!
    @IBOutlet weak var workerView: UIView!
    @IBOutlet weak var bottomImageView: UIImageView!
    @IBOutlet weak var bottomImageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topImageViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var resumeCountLabel: UILabel!
    
    var timer: Timer?
    var actualImageIndex = 0
    let animationDuration = 2.0
    let images = ["pic_welcome_1.jpg", "pic_welcome_2.jpg", "pic_welcome_3.jpg", "pic_welcome_4.jpg"]

    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()
        loadOptions()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        startTimer()
        prepareView()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        stopTimer()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
     //   prepareView()
    }
    
    deinit {
        timer?.invalidate()
        timer = nil
    }
    
    // MARK: Timer
    func startTimer() {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 1 + animationDuration, target: self, selector: #selector(self.loop), userInfo: nil, repeats: true)
        }
    }
    
    func stopTimer() {
        if timer != nil {
            timer?.invalidate()
            timer = nil
        }
    }
    
    func loop() {
        // move top image view back to initial position
        // set image as bottom image view image
        topImageView.image = UIImage.init(named: images[actualImageIndex])
        topImageView.alpha = 1
        topImageViewLeadingConstraint.constant = 0
        bottomImageView.alpha = 1
        // increase actual image index
        actualImageIndex == images.count - 1 ? (actualImageIndex = 0) : (actualImageIndex += 1)
        
        // prepare bottom image view
        bottomImageView.image = UIImage.init(named: images[actualImageIndex])
//        bottomImageView.alpha = 0
        bottomImageViewLeadingConstraint.constant = bottomImageView.frame.size.width
        
        // update layout before animation
        view.layoutIfNeeded()
        
        // set new constants for animation
        topImageViewLeadingConstraint.constant = -bottomImageView.frame.size.width
        bottomImageViewLeadingConstraint.constant = 0
        
        weak var weakSelf = self
        UIView.animate(withDuration: animationDuration, delay: 0, options: .curveEaseIn, animations: {
            weakSelf?.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    // MARK: Methods
    func prepareView() {
        if AppManager.shared.options != nil {
            resumeCountLabel.text = "\(AppManager.shared.options.resumeCount) чел."
            if let user = AppManager.shared.options.user {
                // is user is logged in as an employer
                if user.type == 0 {
                    employerView.isHidden = false
                    workerView.isHidden = true
                    searchingView.isHidden = true
                } else {
                    // if as a worker
                    workerView.isHidden = false
                    employerView.isHidden = true
                    searchingView.isHidden = true
                }
            } else {
                searchingView.isHidden = false
                employerView.isHidden = true
                workerView.isHidden = true
            }
        }
    }
    
    func loadOptions() {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.getOptions(successHandler: { 
            SVProgressHUD.dismiss()
            weakSelf?.prepareView()
        }) { (error) in
            SVProgressHUD.dismiss()
           weakSelf?.alert(message: error.localizedDescription, title: "", handler: { (action) in
                weakSelf?.loadOptions()
            })
        }
    }
    
    @IBAction func sitePressed(_ sender: Any) {
        openURL(urlString: "http://www.MSKJOB.RU")
    }
    
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }

}

// MARK: Actions
extension StartViewController {
    
    @IBAction func workButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(SignInWorkerViewController.init(), animated: true)
    }
    
    @IBAction func staffButtonPressed(_ sender: Any) {
//        navigationController?.pushViewController(SignInEmployerViewController.init(), animated: true)
        let groupsViewController = GroupsViewController.init()
        groupsViewController.applicationMode = .employerMode
        navigationController?.pushViewController(groupsViewController, animated: true)
    }
    
    @IBAction func searchWorkersButtonPressed(_ sender: Any) {
        let vc = GroupsViewController.init()
        vc.applicationMode = .employerMode
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func exitEmployerButtonPressed(_ sender: Any) {
        let alert = LogoutAlertViewController.init()
        alert.modalPresentationStyle = .overFullScreen
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func myResumeButtonPressed(_ sender: Any) {
        navigationController?.pushViewController(ProfileViewController.init(), animated: true)
    }
    
    @IBAction func errorButtonClicked(_ sender: Any) {
        let vc = ErrorAlertController.init()
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
    }

}
