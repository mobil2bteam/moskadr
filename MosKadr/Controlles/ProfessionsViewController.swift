//
//  ProfessionsViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class ProfessionsViewController: UIViewController {

    @IBOutlet weak var professionsTableView: UITableView!
    var group: ProfessionGroupModel!
    let cellIdentifier = "ProfessionTableViewCell"
    var selectedProfessions: [IndexPath] = []
    var phone: String?
    var signUpModel: SignUpModel!

    //MARK: Lifecycle
    convenience init(group: ProfessionGroupModel, applicationMode: ApplicationMode) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.group = group
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        professionsTableView.register(ProfessionTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        professionsTableView.tableFooterView = UIView.init()
        let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
        navigationItem.rightBarButtonItem = rightBarButton
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        if selectedProfessions.count > 0 {
            let professions: [Int] = selectedProfessions.flatMap({ (index) -> Int in
                return group.profession[index.row].id
            })
            if let s = self.signUpModel {
                s.professions = professions
                if phone != nil {
                    signUpModel.phone = phone!
                }
                let salaryViewController = SalaryViewController.init(signUpModel: s)
                navigationController?.pushViewController(salaryViewController, animated: true)
            } else {
                let signUpModel = SignUpModel.init()
                signUpModel.group = group
                signUpModel.professions = professions
                if phone != nil {
                    signUpModel.phone = phone!
                }
                let salaryViewController = SalaryViewController.init(signUpModel: signUpModel)
                navigationController?.pushViewController(salaryViewController, animated: true)
            }
        }
    }
    
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

}

extension ProfessionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group.profession.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ProfessionTableViewCell
        let profession = group.profession[indexPath.row]
        cell.configure(for: profession)
        cell.setCheckmark(checked: selectedProfessions.contains(indexPath))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let index = selectedProfessions.index(of: indexPath) {
            selectedProfessions.remove(at: index)
        } else {
            selectedProfessions.append(indexPath)
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}
