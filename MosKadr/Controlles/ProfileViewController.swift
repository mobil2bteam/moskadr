
//
//  ProfileViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import AlamofireImage
import SVProgressHUD
import SKPhotoBrowser
import MessageUI

class ProfileViewController: UIViewController {
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var emailLabel: UILabel!

    @IBOutlet weak var showPhoneButton: UIButton!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var userBackgroundImageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var aboutView: UIView!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var vacancyLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var metroView: UIView!
    @IBOutlet weak var metroLabel: UILabel!
    @IBOutlet weak var phoneLabel: CopyableLabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var socialView: UIView!
    @IBOutlet weak var vkView: UIView!
    @IBOutlet weak var fbView: UIView!
    @IBOutlet weak var okView: UIView!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var writeView: UIView!
    @IBOutlet weak var vacancyTableView: UITableView!
    @IBOutlet weak var vacancyTableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var resumeSalaryLabel: UILabel!
    @IBOutlet weak var workerSalaryView: UIView!
    @IBOutlet weak var activateButton: UIButton!
    var user = AppManager.shared.options.user // is used when controller shows user profile
    var resume: ResumeModel! // is used when controller show worker's resume
    let cellIdentifier = "VacancyTableViewCell"
    var isResumeMode = false
    
    //MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    convenience init(resume: ResumeModel) {
        self.init()
        self.resume = resume
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showPhoneButton.isHidden = true
        automaticallyAdjustsScrollViewInsets = false
        prepareVacancyTableView()
        if isResumeMode {
            prepareViewForResume()
        } else {
            navigationItem.setHidesBackButton(true, animated:true)
            writeView.isHidden = true
            workerSalaryView.isHidden = true
        }
    }
    
    deinit {
        removeObserver(self, forKeyPath: "vacancyTableView.contentSize")
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "vacancyTableView.contentSize" {
            vacancyTableViewHeightConstraint.constant = vacancyTableView.contentSize.height
            view.layoutIfNeeded()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        prepareView()
        if !isResumeMode {
            navigationController?.setNavigationBarHidden(true, animated: false)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    //MARK: Methods
    func prepareVacancyTableView() {
        vacancyTableView.register(VacancyTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        vacancyTableView.estimatedRowHeight = 50
        vacancyTableView.rowHeight = UITableViewAutomaticDimension
        addObserver(self, forKeyPath: "vacancyTableView.contentSize", options: .new, context: nil)
    }
    
    func prepareView() {
        if isResumeMode {
            return
        }
        user = AppManager.shared.options.user!
        if let metro_id = user?.metro_id {
            let t = AppManager.shared.options.metro.filter{ $0.id == metro_id }
            if t.first != nil {
                metroLabel.text = "Метро: " + t.first!.name!
                metroView.isHidden = false
            } else {
                metroView.isHidden = true
            }
        } else {
            metroView.isHidden = true
        }
        guard let user = user else {
            return
        }
        if user.about.count > 0 {
            aboutView.isHidden = false
            aboutLabel.text = user.about
        } else {
            aboutView.isHidden = true
        }
        let title = user.resumeStatus == 1 ? "Отключить анкету" : "Включить анкету"
        let color = user.resumeStatus == 1 ? UIColor.red : self.editButton.tintColor
        activateButton.setTitleColor(color, for: .normal)
        activateButton.setTitle(title.uppercased(), for: .normal)
        dateLabel.text = "Дата размещения: " + user.date
        vacancyTableView.reloadData()
        phoneLabel.text = user.phone
        emailLabel.text = user.email
        emailView.isHidden = user.email.isEmpty
        nameLabel.text = user.name
        salaryLabel.text = "\(user.salary) руб."
        genderLabel.text = user.sex == 1 ? "Мужчина" : "Женщина"
        genderLabel.text = genderLabel.text! + ", " + user.fomatterdAge
        experienceLabel.text = Experience.value(for: user.experience).value
        let photoUrl = URL(string: user.foto)
        if photoUrl != nil {
            userImageView.af_setImage(withURL: photoUrl!)
            userBackgroundImageView.af_setImage(withURL: photoUrl!)

        }
        guard let nationality = AppManager.shared.options.nationality(for: user.nationality),
            let imageString = nationality.image,
            let url = URL(string: imageString) else {
                return
        }
        flagImageView.af_setImage(withURL: url)
    }

    func prepareViewForResume() {
        emailLabel.textColor = UIColor.blueButtonBackgroundColor()
        if let metro_id = resume.metro_id {
            let t = AppManager.shared.options.metro.filter{ $0.id == metro_id }
            if t.first != nil {
                metroLabel.text = "Метро: " + t.first!.name!
                metroView.isHidden = false
            } else {
                metroView.isHidden = true
            }
        } else {
            metroView.isHidden = true
        }
        activateButton?.removeFromSuperview()
        dateLabel.text = "Дата размещения: " + resume.date
        resumeSalaryLabel.text = "\(resume.salary) руб."
        editButton.isHidden = true
        if resume.about.count > 0 {
            aboutView.isHidden = false
            aboutLabel.text = resume.about
        } else {
            aboutView.isHidden = true
        }
        phoneLabel.text = resume.phone
        print(resume.phone)
        print(resume.phone.count)
        if resume.phone.isEmpty {
            emailView.isHidden = true
            showPhoneButton.isHidden = false
        } else {
            emailLabel.text = resume.email
            emailView.isHidden = resume.email.isEmpty
            showPhoneButton.isHidden = true
        }
        nameLabel.text = resume.name
        salaryLabel.text = "\(resume.salary) руб."
        genderLabel.text = resume.sex == 1 ? "Мужчина" : "Женщина"
        genderLabel.text = genderLabel.text! + ", " + resume.fomatterdAge
        experienceLabel.text = Experience.value(for: resume.experience).value
        userView.isHidden = true
        let photoUrl = URL(string: resume.foto)
        if photoUrl != nil {
            userImageView.af_setImage(withURL: photoUrl!)
            userBackgroundImageView.af_setImage(withURL: photoUrl!)
        }
        guard let nationality = AppManager.shared.options.nationality(for: resume.nationality),
            let imageString = nationality.image,
            let url = URL(string: imageString) else {
                return
        }
        flagImageView.af_setImage(withURL: url)
    }
    
    func openURL(urlString: String) {
        if let url = URL(string: urlString) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    func groups() -> [ProfessionGroupModel] {
        if isResumeMode {
            return resume.groups()
        }
        return user?.groups() ?? []
    }
    
    @IBAction func showPhoneClicked(_ sender: Any) {
        let vc = SignInEmployerViewController.init(nibName: "SignInEmployerViewController", bundle: nil)
        vc.completion = { _ in
            self.loadResume()
        }
        let nav = UINavigationController.init(rootViewController: vc)
        present(nav, animated: true, completion: nil)
    }
    
    @IBAction func updateButtonClicked(_ sender: Any) {
        SVProgressHUD.show()
        APIManager.updateResume { (message) in
            SVProgressHUD.dismiss()
            self.alert(message: message)
            self.prepareView()
        }
    }
    
    @IBAction func emailClicked() {
        if !isResumeMode, let email = resume?.email, !email.isEmpty {
            let composeVC = MFMailComposeViewController()
            composeVC.mailComposeDelegate = self
            composeVC.setToRecipients([email])
            self.present(composeVC, animated: true, completion: nil)
        }
    }
    
    func loadResume() {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.getResume(with: resume.id, successHandler: { (resume) in
            SVProgressHUD.dismiss()
            self.resume = resume
            self.prepareViewForResume()
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
}

extension ProfileViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups().count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! VacancyTableViewCell
        let group = groups()[indexPath.row]
        cell.groupLabel.text = group.name
        cell.professionsLabel.text = group.getAllProfessionsText(separator: "\n")
        return cell
    }
}

//MARK: Actions
extension ProfileViewController {
    
    @IBAction func vkButtonPressed(_ sender: Any) {
        guard let user = user else {
            return
        }
        let urlString = isResumeMode ? resume.vk_link : user.vk_link
        openURL(urlString: urlString)
    }
    
    @IBAction func fbButtonPressed(_ sender: Any) {
        guard let user = user else {
            return
        }
        let urlString = isResumeMode ? resume.fb_link : user.fb_link
        openURL(urlString: urlString)
    }
    
    @IBAction func okButtonPressed(_ sender: Any) {
        guard let user = user else {
            return
        }
        let urlString = isResumeMode ? resume.ok_link : user.ok_link
        openURL(urlString: urlString)
    }
    
    @IBAction func activateButtonClicked(_ sender: Any) {
        guard let user = user else {
            return
        }
        SVProgressHUD.show()
        APIManager.enableResume(user.resumeStatus == 0, successHandler: { (text) in
            SVProgressHUD.dismiss()
            self.user?.resumeStatus = self.user?.resumeStatus == 1 ? 0 : 1
            self.prepareView()
            self.alert(message: text)
        }) { (error) in
            SVProgressHUD.dismiss()
            self.alert(message: error)
        }
    }

    @IBAction func editButtonPressed(_ sender: Any) {
        guard let user = user else {
            return
        }
        let signUpModel = SignUpModel.init(with: user)
        let personalInfoViewController = PersonalInfoViewController.init(signUpModel: signUpModel)
        personalInfoViewController.isEditingUserMode = true
        let navigationController = RPTransitionNavController.init(rootViewController: personalInfoViewController)
        present(navigationController, animated: true, completion: nil)
    }
    
    @IBAction func changePasswordButtonPressed(_ sender: Any) {
        let alert = ChangePasswordViewController.init()
        alert.modalPresentationStyle = .overFullScreen
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func exitButtonPressed(_ sender: Any) {
        let alert = LogoutAlertViewController.init()
        alert.modalPresentationStyle = .overFullScreen
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func inviteButtonPressed(_ sender: Any) {
        if AppManager.shared.options.user == nil {
            let vc = SignInEmployerViewController.init(nibName: "SignInEmployerViewController", bundle: nil)
            vc.completion = { _ in
                self.loadResume()
            }
            vc.showAuthMessage = true
            let nav = UINavigationController.init(rootViewController: vc)
            present(nav, animated: true, completion: nil)
            return
        }
        if resume.phone.count == 0 && resume.email.count == 0 {
            return
        }
        let alert = InvitationViewController.init(resume: resume)
        alert.modalPresentationStyle = .overFullScreen
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeAvatarButtonPressed(_ sender: Any) {
        if !isResumeMode {
            guard let user = user else {
                return
            }
            weak var weakSelf = self
            let model = SignUpModel.init(with: AppManager.shared.options.user!)
            let vc = ChangeAvatarViewController.init(model: model)
            vc.imageUrl = user.foto
            vc.competionBlock = {
                weakSelf?.prepareView()
            }
            let navVC = UINavigationController.init(rootViewController: vc)
            navVC.modalPresentationStyle = .overFullScreen
            navVC.modalTransitionStyle = .crossDissolve
            present(navVC, animated: true, completion: nil)
        } else {
            if resume.foto.isEmpty { return }
            var images = [SKPhoto]()
            let photo = SKPhoto.photoWithImageURL(resume.foto)
            photo.shouldCachePhotoURLImage = false // you can use image cache by true(NSCache)
            images.append(photo)
            
            let browser = SKPhotoBrowser(photos: images)
            browser.initializePageIndex(0)
            present(browser, animated: true, completion: {})
            //            let vc = PhotoFullScreeViewController.init(imageUrl: resume.foto)
            //            let navVC = UINavigationController.init(rootViewController: vc)
            //            navVC.modalPresentationStyle = .overFullScreen
            //            navVC.modalTransitionStyle = .crossDissolve
            //            present(navVC, animated: true, completion: nil)
        }
    }

    
}

extension ProfileViewController: MFMailComposeViewControllerDelegate {
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
