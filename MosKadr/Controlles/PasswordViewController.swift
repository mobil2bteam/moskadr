//
//  PasswordViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD
import Material

class PasswordViewController: UIViewController {
    
    var signUpModel: SignUpModel!
    
    @IBOutlet weak var passwordTextField: ErrorTextField!
    @IBOutlet weak var confirmPasswordTextField: ErrorTextField!

    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
        navigationItem.rightBarButtonItem = rightBarButton
    }

    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }
    

    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        if (passwordTextField.text!.count == 0) {
            passwordTextField.detail = "Введите пароль"
            passwordTextField.isErrorRevealed = true
            passwordTextField.shake()
            return
        }
        passwordTextField.isErrorRevealed = false
        if (confirmPasswordTextField.text!.count == 0) {
            confirmPasswordTextField.detail = "Введите пароль для подтверждения"
            confirmPasswordTextField.isErrorRevealed = true
            confirmPasswordTextField.shake()
            return
        }
        confirmPasswordTextField.isErrorRevealed = false
        let password = passwordTextField.text!
        let confirmPassword = confirmPasswordTextField.text!
        if password != confirmPassword {
            confirmPasswordTextField.detail = "Пароли не совпадают"
            confirmPasswordTextField.isErrorRevealed = true
            passwordTextField.shake()
            confirmPasswordTextField.shake()
            return
        }
        confirmPasswordTextField.isErrorRevealed = false
        signUpModel.password = password
        signUp()
    }

    //MARK: Methods
    func signUp() {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.signUp(parameters: signUpModel.getSignUpParameters(), image: signUpModel.image, successHandler: {
            SVProgressHUD.dismiss()
            weakSelf?.finishSignUp()
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    func finishSignUp() {
        navigationController?.pushViewController(SignUpFinishViewController.init(), animated: true)
    }
}

extension PasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == passwordTextField {
            _ = confirmPasswordTextField.becomeFirstResponder()
        }
        return true
    }
    
}
