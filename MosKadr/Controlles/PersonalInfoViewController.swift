//
//  PersonalInfoViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import AlamofireImage
import Material

class PersonalInfoViewController: UIViewController {
    var signUpModel: SignUpModel!
    var birthDate: String?
    var nationality: NationalityModel?
    var experience: ExperienceTuple?
    var isEditingUserMode = false
    @IBOutlet weak var nameTextField: CustomTextField!
    @IBOutlet weak var flagImageView: UIImageView!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var nationalityTextField: UITextField!
    @IBOutlet weak var experienceTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!

    
    @IBOutlet weak var genderSegmentControl: UISegmentedControl!
    @IBOutlet weak var statusLabel: UILabel!
    
    var yearsArray: [Int] {
        get {
            var temp = [Int]()
            let dateFormatter = DateFormatter.init(withFormat: "yyyy", locale: "ru_RU")
            let currentYear = Int(dateFormatter.string(from: Date()))!
            for i in 1930...currentYear - 16{
                temp.append(i)
            }
            temp.reverse()
            return temp
        }
    }
    
    lazy var datePicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()

    lazy var nationalityPicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    lazy var experiencePicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()


    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureAllViews()
        setDefaultBirthday()
        setDefaultExperience()
        setDefaultNationality()
        
        if let e = signUpModel.experience {
            experience = e
            experienceTextField.text = e.value
        }
        if let e = signUpModel.email {
            emailTextField.text = e
        }
        if let b = signUpModel.birthDate {
            birthDate = b
            birthdayTextField.text = b
            if let index = yearsArray.index(of: Int(signUpModel.birthDate) ?? yearsArray.last!) {
                datePicker.selectRow(index, inComponent: 0, animated: false)
            }
        }
        if let currentNationality = AppManager.shared.options.nationality(for: signUpModel.nationality) {
            nationalityTextField.text = currentNationality.name
            if currentNationality.image != nil {
                if let imageUrl = URL(string: currentNationality.image!) {
                    flagImageView.af_setImage(withURL: imageUrl)
                }
            }
        }
        if let n = signUpModel.name {
            nameTextField.text = n
        }
        if let g = signUpModel.isMan {
            genderSegmentControl.selectedSegmentIndex = g == true ? 0 : 1
        }
        if isEditingUserMode {
            statusLabel.text = "РЕДАКТИРОВАНИЕ"
            experience = signUpModel.experience
            birthDate = signUpModel.birthDate
            if let currentNationality = AppManager.shared.options.nationality(for: signUpModel.nationality) {
                nationalityTextField.text = currentNationality.name
                if currentNationality.image != nil {
                    if let imageUrl = URL(string: currentNationality.image!) {
                        flagImageView.af_setImage(withURL: imageUrl)
                    }
                }
            }
            genderSegmentControl.selectedSegmentIndex = signUpModel.isMan == true ? 0 : 1
            birthdayTextField.text = signUpModel.birthDate
            if let index = yearsArray.index(of: Int(signUpModel.birthDate) ?? yearsArray.last!) {
                datePicker.selectRow(index, inComponent: 0, animated: false)
            }
            nameTextField.text = signUpModel.name
            experienceTextField.text = experience?.value
            // add close bar button
            let rightBarButton = UIBarButtonItem.init(title: "Отменить", style: .done, target: self, action: #selector(self.rightButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        } else {
            let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        }
    }
    
    func rightButtonAction(sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

    
    //MARK: Methods
    func configureAllViews() {
        birthdayTextField.inputView = datePicker
        datePicker.delegate = self
        datePicker.dataSource = self
        
        nationalityTextField.inputView = nationalityPicker
        nationalityPicker.dataSource = self
        nationalityPicker.delegate = self
        
        experienceTextField.inputView = experiencePicker
        experiencePicker.dataSource = self
        experiencePicker.delegate = self
    }

    func setDefaultNationality() {
        if AppManager.shared.options.nationality.count > 0 {
            let firstNationality = AppManager.shared.options.nationality[0]
            nationality = firstNationality
            nationalityTextField.text = firstNationality.name
            flagImageView.image = nil
            if firstNationality.image != nil {
                if let imageUrl = URL(string: firstNationality.image!) {
                    flagImageView.af_setImage(withURL: imageUrl)
                }
            }
        }
    }
    
    func setDefaultExperience() {
        experience = Experience.values[0]
        experienceTextField.text = experience?.value
    }
    
    func setDefaultBirthday() {
        let date = "\(yearsArray.first!)"
        birthdayTextField.text = "\(yearsArray.first!)"
        birthDate = date
    }

    //MARK: Actions
    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        nameTextField.isErrorRevealed = nameTextField.text!.count < 1
        if (nameTextField.text!.count < 1) {
            nameTextField.shake()
            return
        }
        signUpModel.birthDate = birthDate!
        signUpModel.name = nameTextField.text!
        signUpModel.email = emailTextField.text
        signUpModel.nationality = nationality!.id
        signUpModel.experience = experience
        signUpModel.isMan = genderSegmentControl.selectedSegmentIndex == 0
        
        let contactsViewController = ContactsViewController.init(signUpModel: signUpModel)
        contactsViewController.isEditingUserMode = isEditingUserMode
        navigationController?.pushViewController(contactsViewController, animated: true)
    }

}

extension PersonalInfoViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        if pickerView == nationalityPicker {
            let myView = UIView(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 44))
            let myImageView = UIImageView(frame: CGRect(x: pickerView.bounds.width - 60, y: 0, width: 30, height: 44))
            let myLabel = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 60, height: 44))
            myLabel.textAlignment = .center
            myView.addSubview(myImageView)
            myView.addSubview(myLabel)
            myView.translatesAutoresizingMaskIntoConstraints = true
            myImageView.contentMode = .scaleAspectFit
            
            let nationality = AppManager.shared.options.nationality[row]
            myLabel.text = nationality.name
            guard let image = nationality.image,
            let url =  URL(string: image) else {
                myImageView.image = UIImage.init(named: "flag_placeholder")
                return myView
            }
            myImageView.af_setImage(withURL: url)
            return myView
        }
        if pickerView == experiencePicker || pickerView == datePicker {
            let myLabel = UILabel(frame: CGRect(x: 0, y: 0, width: pickerView.bounds.width - 30, height: 44))
            myLabel.textAlignment = .center
            myLabel.text = pickerView == experiencePicker ? Experience.values[row].value : "\(yearsArray[row])"
            return myLabel
        }
        return UIView.init()
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == nationalityPicker {
            return AppManager.shared.options.nationality.count
        }
        if pickerView == experiencePicker {
            return Experience.values.count
        }
        if pickerView == datePicker {
            return yearsArray.count
        }
        return 0
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 44
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == experiencePicker {
            experience = Experience.values[row]
            experienceTextField.text = experience?.value
        }
        if pickerView == datePicker {
            let date = "\(yearsArray[row])"
            birthdayTextField.text = date
            birthDate = date
        }
        if pickerView == nationalityPicker {
            let nationality = AppManager.shared.options.nationality[row]
            self.nationality = nationality
            nationalityTextField.text = nationality.name
            guard let image = nationality.image,
                let url =  URL(string: image) else {
                    flagImageView.image = UIImage.init(named: "flag_placeholder")
                    return
            }
            flagImageView.af_setImage(withURL: url)
        }
    }
}

extension PersonalInfoViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        (textField as? ErrorTextField)?.isErrorRevealed = textField.text!.count < 6
        return true
    }
}
