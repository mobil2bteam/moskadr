//
//  SalaryViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Material

class SalaryViewController: UIViewController {
    @IBOutlet weak var salaryTextField: ErrorTextField!
    @IBOutlet weak var salaryView: UIView!
    var signUpModel: SignUpModel!
    var isEditingUserMode = false
    @IBOutlet weak var statusLabel: UILabel!

    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        salaryTextField.font = UIFont.boldSystemFont(ofSize: 30)
        salaryTextField.textAlignment = .center
        salaryTextField.detail = "Введите размер зарплаты"
        if signUpModel.salary > 0 {
            salaryTextField.text = "\(signUpModel.salary)"
        }
        if isEditingUserMode {
            statusLabel.text = "РЕДАКТИРОВАНИЕ"
            salaryTextField.text = "\(signUpModel.salary)"
        } else {
            let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if salaryTextField.text!.isEmpty {
            _ = salaryTextField.becomeFirstResponder()
        }
    }
    
    //MARK: Actions
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        salaryTextField.isErrorRevealed = salaryTextField.text!.count == 0
        if (salaryTextField.text!.count == 0) {
            salaryView.shake()
            return
        }
        if let salary = Int(salaryTextField.text!) {
            signUpModel.salary = salary
            if isEditingUserMode {
                let editProfessionsViewController = EditProfessionsViewController.init(signUpModel: signUpModel)
                navigationController?.pushViewController(editProfessionsViewController, animated: true)
            } else {
                let personalInfoViewController = PersonalInfoViewController.init(signUpModel: signUpModel)
                navigationController?.pushViewController(personalInfoViewController, animated: true)
            }
        }
    }
}

extension SalaryViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string != "" && textField.text?.count == 7 {
            return false
        }
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }
}
