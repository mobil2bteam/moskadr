//
//  EditProfessionsViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/19/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD

class EditProfessionsViewController: UIViewController {

    var signUpModel: SignUpModel!
    var allGroups = AppManager.shared.options.profession_group
    let cellIdentifier = "GroupEditTableViewCell"
    @IBOutlet weak var groupsTableView: UITableView!
    
    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        groupsTableView.estimatedRowHeight = 50
        groupsTableView.rowHeight = UITableViewAutomaticDimension
        groupsTableView.register(GroupEditTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        groupsTableView.tableFooterView = UIView.init()
    }
    
    // MARK: Actions
    @IBAction func nextButtonPressed(_ sender: Any) {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.updateUser(parameters: signUpModel.getUpdateParameters(), image: signUpModel.image, successHandler: {
            SVProgressHUD.dismiss()
            weakSelf?.dismiss(animated: true, completion: nil)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    // MARK: Helpers
    // get text from names all choosen groups joining with separator
    fileprivate func allChoosenProfessionText(for group: ProfessionGroupModel, separator: String = ", ") -> String {
        var names: [String] = []
        for p in group.profession {
            for id in signUpModel.professions {
                if p.id == id {
                    names.append(p.name)
                }
            }
        }
        return names.joined(separator: separator)
    }
}

extension EditProfessionsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allGroups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GroupEditTableViewCell
        let group = allGroups[indexPath.row]
        cell.configure(for: group)
        cell.professionsLabel.text = allChoosenProfessionText(for: group)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let group = allGroups[indexPath.row]
        let profession = signUpModel.professions

        weak var weakSelf = self
        let alert = ProfessionsAlertViewController.init(professions: profession, group: group)
        alert.completionHandler = { professions in
            weakSelf?.signUpModel.professions = professions
            weakSelf?.groupsTableView.reloadData()
        }
        let navVC = UINavigationController.init(rootViewController: alert)
        present(navVC, animated: true, completion: nil)
    }
}
