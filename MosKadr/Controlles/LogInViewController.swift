//
//  LogInViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD
import Material

class LogInViewController: UIViewController {

    @IBOutlet weak var phoneTextField: TextField!
    @IBOutlet weak var passwordTextField: ErrorTextField!
    @IBOutlet weak var nextButton: MainButton!
    var phone: String!
    
    
    // MARK: Lifecycle
    convenience init(phone: String) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.phone = phone
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.text = phone
    }

    // MARK: - Actions
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        let alert = RestorePasswordViewController.init()
        alert.modalPresentationStyle = .overFullScreen
        alert.modalTransitionStyle = .crossDissolve
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        passwordTextField.isErrorRevealed = passwordTextField.text!.count == 0
        if (passwordTextField.text!.count == 0) {
            passwordTextField.shake()
            return
        }
        weak var weakSelf = self
        nextButton.isLoading = true
        APIManager.signIn(login: phone, password: passwordTextField.text!, type: 1, successHandler: {
            weakSelf?.nextButton.isLoading = false
            weakSelf?.showProfileViewController()
        }) { (error) in
            weakSelf?.nextButton.isLoading = false
            weakSelf?.alert(message: error)
        }
    }

    // MARK: - Methods
    func showProfileViewController() {
        let profileViewController = ProfileViewController.init()
        navigationController?.pushViewController(profileViewController, animated: true)
    }
}

extension LogInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
}
