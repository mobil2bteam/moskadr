//
//  WorkerListViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD

class WorkerListViewController: UIViewController {
    var resultInfo: ResulInfoModel! {
        didSet {
            numberLabel.text = "Найдено \(resultInfo.filter.resume_total) резюме"
            workersTableView.reloadData()
        }
    }
    let cellIdentifier = "WorkerTableViewCell"
    var actualParameters = [String: String]()
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var workersTableView: UITableView!
    
    //MARK: Lifecycle
    convenience init(result: ResulInfoModel, parameters: [String: String]) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.resultInfo = result
        self.actualParameters = parameters
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberLabel.text = "Найдено \(resultInfo.filter.resume_total) резюме"
        workersTableView.register(WorkerTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        workersTableView.tableFooterView = UIView.init()
    }
    
    //MARK: Actions
    @IBAction func filterButtonPressed(_ sender: Any) {
        weak var weakSelf = self
        let vc = FilterViewController.init(result: resultInfo)
        vc.filterBlock = { parameters in
            weakSelf?.loadVacancies(parameters: parameters)
        }
        navigationController?.pushViewController(vc, animated: true)
    }

    //MARK: Methods
    func loadVacancies(parameters: [String: String], loadMore: Bool = false) {
        actualParameters = parameters
        weak var weakSelf = self
        if !loadMore {
            SVProgressHUD.show()
        }
        APIManager.getWorkers(with: parameters, successHandler: { (result) in
            SVProgressHUD.dismiss()
            if loadMore {
                weakSelf?.resultInfo.filter = result.filter
                var newIndexPaths = [IndexPath]()
                for newResult in result.results {
                    newIndexPaths.append(IndexPath(row: (weakSelf?.resultInfo.results.count)!, section: 0))
                    weakSelf?.resultInfo.results.append(newResult)
                }
                weakSelf?.workersTableView.insertRows(at: newIndexPaths, with: .left)
            } else {
                weakSelf?.resultInfo = result
                weakSelf?.workersTableView.reloadData()
            }
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }

    func loadResume(with id: Int) {
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.getResume(with: id, successHandler: { (resume) in
            SVProgressHUD.dismiss()
            weakSelf?.showResumeViewController(resume: resume)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    //MARK: Nagiation
    func showResumeViewController(resume: ResumeModel) {
        let vc = ProfileViewController.init()
        vc.resume = resume
        vc.isResumeMode = true
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension WorkerListViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return resultInfo.results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! WorkerTableViewCell
        let result = resultInfo.results[indexPath.row]
        cell.configure(for: result)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        loadResume(with: resultInfo.results[indexPath.row].id)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = resultInfo.results.count - 1
        if indexPath.row == lastElement && resultInfo.filter.page < resultInfo.filter.page_count {
            actualParameters["page"] = "\(resultInfo.filter.page + 1)"
            loadVacancies(parameters: actualParameters, loadMore: true)
        }
    }

}
