//
//  ContactsViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/18/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Material
import SVProgressHUD

class ContactsViewController: UIViewController {
    var signUpModel: SignUpModel!
    var isEditingUserMode = false
    @IBOutlet weak var phoneTextField: TextField!
    //@IBOutlet weak var emailTextField: ErrorTextField!
    @IBOutlet weak var userSwitch: UISwitch!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var agreementView: UIView!
    @IBOutlet weak var metroView: UIView!
    @IBOutlet weak var metroButton: UIButton!

    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        phoneTextField.text = signUpModel.phone
       
        if let m = signUpModel.metro_id {
            let t = AppManager.shared.options.metro.filter{$0.id == m}
            if t.first != nil {
                self.metroButton.setTitle(t.first!.name, for: .normal)
            }
        }
        
        if isEditingUserMode {
            if let metro = signUpModel.metro_id {
                let t = AppManager.shared.options.metro.filter{$0.id == metro}
                if t.first != nil {
                    self.metroButton.setTitle(t.first!.name, for: .normal)
                }
            }
            statusLabel.text = "РЕДАКТИРОВАНИЕ"
        //    emailTextField.text = signUpModel.email
            agreementView.isHidden = true
            userSwitch.isOn = true
        } else {
            let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
            navigationItem.rightBarButtonItem = rightBarButton
        }
    }

    // MARK: Actions
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func metroButtonClicked(_ sender: Any) {
        let vc = MetroListViewController.init(nibName: "MetroListViewController", bundle: nil)
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.completion = { metro in
            self.signUpModel.metro_id = metro.id
            self.metroButton.setTitle(metro.name, for: .normal)
        }
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        /*
        emailTextField.isErrorRevealed = emailTextField.text!.characters.count == 0
        if (emailTextField.text!.characters.count == 0) {
            emailTextField.shake()
            return
        } */
        if !userSwitch.isOn {
            SVProgressHUD.showError(withStatus: "Разрешите использовать ваши данные")
            return
        }
    //    signUpModel.email = emailTextField.text!
        signUpModel.phone = phoneTextField.text!
//        let socialLinksViewController = SocialLinksViewController.init(signUpModel: signUpModel)
//        socialLinksViewController.isEditingUserMode = isEditingUserMode
//        navigationController?.pushViewController(socialLinksViewController, animated: true)
        let vc = AboutViewController.init(signUpModel: signUpModel)
        vc.isEditingUserMode = isEditingUserMode
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension ContactsViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
