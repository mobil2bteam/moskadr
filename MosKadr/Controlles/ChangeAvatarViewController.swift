//
//  ChangeAvatarViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/25/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD
import AlamofireImage

class ChangeAvatarViewController: UIViewController {
    var model: SignUpModel!
    var imagePicker: UIImagePickerController!
    var competionBlock: (() -> Void) = { _ in }
    var imageUrl: String! = ""
    
    @IBOutlet weak var imageView: UIImageView!
    
    //MARK: Lifecycle
    convenience init(model: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.model = model
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        let photoUrl = URL(string: imageUrl)
        if photoUrl != nil {
            imageView.af_setImage(withURL: photoUrl!)
        }
    }

    @IBAction func acceptButtonPressed(_ sender: Any) {
        if model.image == nil {
            imageView.shake()
            return
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.updateUser(parameters: model.getUpdateParameters(),image: model.image, successHandler: {
            SVProgressHUD.dismiss()
            weakSelf?.competionBlock()
            weakSelf?.dismiss(animated: true, completion: nil)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func choosePhotoButtonPressed(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
    
    @IBAction func takePhotoButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
}

extension ChangeAvatarViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            model.image = originalImage
            imageView.image = originalImage
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}
