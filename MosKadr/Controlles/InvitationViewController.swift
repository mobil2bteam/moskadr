//
//  InvitationViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/25/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import MessageUI

class InvitationViewController: UIViewController {
    var resume: ResumeModel!
    
    @IBOutlet weak var callButton: MainButton!
    @IBOutlet weak var smsButton: MainButton!
    @IBOutlet weak var emailButton: MainButton!
    var inviteText: String {
        get {
            let employerName = AppManager.shared.options.user!.name!
            let workerName = resume.name!
            let employerPhone = AppManager.shared.options.user!.phone!
            return  "Здравствуйте, \(workerName)\nВаше резюме показалось нам очень интересным. Мы хотели бы пригласить Вас на cобеседование. Перезвоните, пожалуйста, в рабочее время по телефону 8\(employerPhone)\nС уважением, \(employerName)"
        }
    }
    
    //MARK: Lifecycle
    convenience init(resume: ResumeModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.resume = resume
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        if resume.phone.count == 0 {
            callButton.isHidden = true
            smsButton.isHidden = true
        }
        if resume.email.count == 0 {
            emailButton.isHidden = true
        }
    }
    
    //MARK: Actions
    @IBAction func mailButtonPressed(_ sender: Any) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([resume.email])
        composeVC.setMessageBody(inviteText, isHTML: false)
        self.present(composeVC, animated: true, completion: nil)
    }
    
    @IBAction func smsButtonPressed(_ sender: Any) {
        print(inviteText)
        if (MFMessageComposeViewController.canSendText()) {
            let controller = MFMessageComposeViewController()
            controller.body = inviteText
            controller.recipients = [resume.phone]
            controller.messageComposeDelegate = self
            present(controller, animated: true, completion: nil)
        }
    }
    
    @IBAction func callButtonPressed(_ sender: Any) {
        if let url = URL(string: "telprompt://\(resume.phone)"), UIApplication.shared.canOpenURL(url) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url)
            } else {
                UIApplication.shared.openURL(url)
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}

extension InvitationViewController: MFMessageComposeViewControllerDelegate, MFMailComposeViewControllerDelegate {
    
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
