//
//  ChangePasswordViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Material
import SVProgressHUD

class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var oldPasswordTextField: ErrorTextField!
    @IBOutlet weak var newPasswordTextField: ErrorTextField!

    //MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }
    
    //MARK: Actions
    @IBAction func changeButtonPressed(_ sender: Any) {
        view.endEditing(true)
        oldPasswordTextField.isErrorRevealed = oldPasswordTextField.text!.count == 0
        if (oldPasswordTextField.text!.count == 0) {
            oldPasswordTextField.shake()
            return
        }
        newPasswordTextField.isErrorRevealed = newPasswordTextField.text!.count == 0
        if (newPasswordTextField.text!.count == 0) {
            newPasswordTextField.shake()
            return
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        let token = AppManager.shared.options.user!.token
        APIManager.changePassword(oldPassword: oldPasswordTextField.text!, newPassword: newPasswordTextField.text!, token: token, successHandler: { 
            SVProgressHUD.dismiss()
            weakSelf?.dismiss(animated: true, completion: nil)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}

extension ChangePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == oldPasswordTextField {
            _ = newPasswordTextField.becomeFirstResponder()
        }
        return true
    }
}
