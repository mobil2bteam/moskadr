//
//  GroupsViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/17/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import SVProgressHUD

enum ApplicationMode: String {
    case workerMode = "workerMode"
    case employerMode = "employerMode"
}

class GroupsViewController: UIViewController {
    @IBOutlet weak var groupsTableView: UITableView!
    @IBOutlet weak var statusLabel: UILabel!
    var groups = AppManager.shared.options.profession_group
    var applicationMode: ApplicationMode = .workerMode
    var phone: String? // uses when user when user is signing up
    let cellIdentifier = "GroupTableViewCell"

    //MARK: Lifecycle
    convenience init(applicationMode: ApplicationMode) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.applicationMode = applicationMode
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        groupsTableView.register(GroupTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        groupsTableView.tableFooterView = UIView.init()
        if applicationMode == .employerMode {
            statusLabel.isHidden = true
            navigationItem.setHidesBackButton(true, animated:true)
        }
        let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //MARK: Methods
    func loadVacancies(by groupId: Int) {
        let token = AppManager.shared.options.user?.token
        var parameters = ["profession_group": "\(groupId)"]
        if token != nil {
            parameters["token"] = token!
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.getWorkers(with: parameters, successHandler: { (result) in
            SVProgressHUD.dismiss()
            weakSelf?.showWorkerListViewController(resultInfo: result, parameters: parameters)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    //MARK: Navigation
    func showWorkerListViewController(resultInfo: ResulInfoModel, parameters: [String: String]) {
        let vc = WorkerListViewController.init(result: resultInfo, parameters: parameters)
        navigationController?.pushViewController(vc, animated: true)
    }
        
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

}

extension GroupsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return groups.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! GroupTableViewCell
        let group = groups[indexPath.row]
        cell.configure(for: group)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let group = groups[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        if applicationMode == .workerMode {
            let professionsViewController = ProfessionsViewController.init(group: group, applicationMode: applicationMode)
            let signUpModel = SignUpModel.init()
            signUpModel.phone = phone
            signUpModel.group = group
            professionsViewController.signUpModel = signUpModel
            navigationController?.pushViewController(professionsViewController, animated: true)
        } else {
            loadVacancies(by: group.id)
        }
    }
}
