//
//  RestorePasswordViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Material
import SVProgressHUD

class RestorePasswordViewController: UIViewController {
    
    var type = 1
    @IBOutlet weak var emailTextField: ErrorTextField!
    
    //MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    //MARK: Actions
    @IBAction func restoreButtonPressed(_ sender: Any) {
        view.endEditing(true)
        emailTextField.isErrorRevealed = emailTextField.text!.count == 0
        if (emailTextField.text!.count == 0) {
            emailTextField.shake()
            return
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.restorePassword(email: emailTextField.text!, type: type, successHandler: { (message) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: message.text, title: message.title, handler: { (action) in
                weakSelf?.dismiss(animated: true, completion: nil)
            })
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension RestorePasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}
