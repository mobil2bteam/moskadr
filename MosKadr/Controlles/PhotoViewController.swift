//
//  PhotoViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/19/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {
    var signUpModel: SignUpModel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var userImageView: UIImageView!
    var imagePicker: UIImagePickerController!
    
    //MARK: Lifecycle
    convenience init(signUpModel: SignUpModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.signUpModel = signUpModel
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let i = signUpModel.image {
            userImageView.image = i
        }
        let rightBarButton = UIBarButtonItem.init(title: "На главную", style: .plain, target: self, action: #selector(self.homeButtonAction))
        navigationItem.rightBarButtonItem = rightBarButton
    }
    
    //MARK: Actions
    func homeButtonAction(sender: UIBarButtonItem) {
        navigationController?.popToRootViewController(animated: true)
    }

    @IBAction func nextButtonPressed(_ sender: Any) {
        let passwordViewController = PasswordViewController.init(signUpModel: signUpModel)
        navigationController?.pushViewController(passwordViewController, animated: true)
    }

    @IBAction func photoButtonPressed(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            imagePicker =  UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func libraryButtonPressed(_ sender: Any) {
        imagePicker =  UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true, completion: nil)
    }
}

extension PhotoViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let originalImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userImageView.image = originalImage
            signUpModel.image = originalImage
        } else {
            
        }
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
}
