//
//  SignInEmployerViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/16/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit
import Material
import Alamofire
import SVProgressHUD

class SignInEmployerViewController: UIViewController {
    var showAuthMessage = false
    var completion: (() -> ())?
    @IBOutlet weak var passwordTextField: ErrorTextField!
    @IBOutlet weak var emailTextField: ErrorTextField!
    
    // MARK: Lifecycle
    convenience init() {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let close = UIBarButtonItem.init(title: "Отменить", style: .plain, target: self, action: #selector(closeClicked))
        navigationItem.leftBarButtonItem = close
        if showAuthMessage {
            alert(message: "Данные будут доступны после авторизации")
        }
    }
    
    @objc func closeClicked() {
        dismiss(animated: true, completion: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    // MARK: Actions
    @IBAction func forgotPasswordButtonPressed(_ sender: Any) {
        view.endEditing(true)
        emailTextField.isErrorRevealed = emailTextField.text!.count == 0
        if (emailTextField.text!.count == 0) {
            emailTextField.shake()
            return
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.restorePassword(email: emailTextField.text!, type: 0, successHandler: { (message) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: message.text, title: message.title, handler: nil)
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    @IBAction func nextButtonPressed(_ sender: Any) {
        view.endEditing(true)
        emailTextField.isErrorRevealed = emailTextField.text!.count == 0
        if (emailTextField.text!.count == 0) {
            emailTextField.shake()
            return
        }
        passwordTextField.isErrorRevealed = passwordTextField.text!.count < 4
        if (passwordTextField.text!.count < 4) {
            passwordTextField.shake()
            return
        }
        weak var weakSelf = self
        SVProgressHUD.show()
        APIManager.signIn(login: emailTextField.text!, password: passwordTextField.text!, type: 0, successHandler: {
            SVProgressHUD.dismiss()
            weakSelf?.completion?()
            weakSelf?.dismiss(animated: true, completion: nil)
//            weakSelf?.showGroupsViewController(phone: "")
        }) { (error) in
            SVProgressHUD.dismiss()
            weakSelf?.alert(message: error)
        }
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        view.endEditing(true)
        let vc = SignUpEmployerViewController.init(nibName: "SignUpEmployerViewController", bundle: nil)
        vc.completion = completion
        navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: Navigation
    func showGroupsViewController(phone: String) {
        let groupsViewController = GroupsViewController.init()
        groupsViewController.phone = phone
        groupsViewController.applicationMode = .employerMode
        navigationController?.pushViewController(groupsViewController, animated: true)
    }

}

extension SignInEmployerViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            _ = passwordTextField.becomeFirstResponder()
        }
        textField.resignFirstResponder()
        return true
    }
}
