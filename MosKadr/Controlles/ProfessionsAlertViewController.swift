//
//  ProfessionsAlertViewController.swift
//  MosKadr
//
//  Created by Ruslan on 10/20/17.
//  Copyright © 2017 Renat Ganiev. All rights reserved.
//

import UIKit

class ProfessionsAlertViewController: UIViewController {
    var professions: [Int] = []
    var group: ProfessionGroupModel!
    let cellIdentifier = "ProfessionEditTableViewCell"
    @IBOutlet weak var professionTableView: UITableView!
    var completionHandler: ((_ professions:[Int]) -> Void) = { _ in }

    //MARK: Lifecycle
    convenience init(professions: [Int], group: ProfessionGroupModel) {
        let nibName = String(describing: type(of: self))
        self.init(nibName: nibName, bundle: nil)
        self.group = group
        self.professions = professions
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Профессии"
        professionTableView.register(ProfessionEditTableViewCell.nib(), forCellReuseIdentifier: cellIdentifier)
        professionTableView.tableFooterView = UIView.init()
        let cancel = UIBarButtonItem.init(title: "Отменить", style: .plain, target: self, action: #selector(cancelButtonPressed(_:)))
        navigationItem.leftBarButtonItem = cancel
     
        let save = UIBarButtonItem.init(title: "Применить", style: .plain, target: self, action: #selector(saveButtonPressed(_:)))
        navigationItem.rightBarButtonItem = save

    }

    //MARK: Actions
    @IBAction func saveButtonPressed(_ sender: Any) {
        completionHandler(professions)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}

extension ProfessionsAlertViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return group.profession.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ProfessionEditTableViewCell
        let profession = group.profession[indexPath.row]
        cell.professionLabel.text = profession.name
        cell.setCheckmark(checked: professions.contains(profession.id))
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let profession = group.profession[indexPath.row]
        if let index = professions.index(of: profession.id) {
            professions.remove(at: index)
        } else {
            professions.append(profession.id)
        }
        tableView.reloadRows(at: [indexPath], with: .none)
    }
}
