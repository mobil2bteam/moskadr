
#import "RPPushTransition.h"

@implementation RPPushTransition

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext
{
    return 0.4f;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext
{
    
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    [transitionContext containerView].backgroundColor = [UIColor whiteColor];
    toViewController.view.frame = [transitionContext finalFrameForViewController:toViewController];
    [[transitionContext containerView] addSubview:toViewController.view];
    
    CGRect toFrame = toViewController.view.frame;
    toFrame.origin.x = toFrame.size.width;
    toViewController.view.frame = toFrame;
    toFrame.origin.x = 0;

    CGRect fromFrame = fromViewController.view.frame;
    fromFrame.origin.x = -fromFrame.size.width;
    
//    [UIView animateWithDuration:[self transitionDuration:transitionContext] delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//        fromViewController.view.frame = fromFrame;
//        toViewController.view.frame = toFrame;
//
//    } completion:^(BOOL finished) {
//        [fromViewController.view removeFromSuperview];
//        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//    }];
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        fromViewController.view.frame = fromFrame;
        toViewController.view.frame = toFrame;
    } completion:^(BOOL finished) {
        [fromViewController.view removeFromSuperview];
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
    }];
}

@end
